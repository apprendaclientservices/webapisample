﻿var myApp = angular.module('myApp', []);

myApp.controller('WebApiCtrl', function ($scope, $http) {

    $http.get('/api/v1/test').success(function(response) {
        $scope.result1 = response;
    });
    
    $http.get('/api/v1/test2').success(function (response) {
        $scope.result2 = response;
    });
});