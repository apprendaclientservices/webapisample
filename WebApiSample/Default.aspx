﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebApiSample.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" ng-app="myApp">
<head runat="server">
    <title>Test App</title>
    <script src="Scripts/angular.js"></script>
    <script src="Scripts/app.js"></script>
</head>
<body ng-controller="WebApiCtrl">
    
    <div>
        Response from api/v1/test1: id: "{{result1.id}}" value: "{{result1.value}}"
    </div>
        <div>
        Response from api/v1/test2: id: "{{result2.id}}" value: "{{result2.value}}"
    </div>

</body>
</html>
