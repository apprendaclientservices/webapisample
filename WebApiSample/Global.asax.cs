﻿using System;
using System.Web.Http;
using WebApi.Basic;


namespace WebApiSample
{
    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {
            WebApiBase.Initialize(GlobalConfiguration.Configuration);
        }

    }
}