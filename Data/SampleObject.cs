﻿namespace WebApiSample.Data
{
    public class SampleObject
    {
        public int Id { get; set; }
        public string Value { get; set; }
    }
}
