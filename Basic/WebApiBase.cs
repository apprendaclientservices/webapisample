﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Text.RegularExpressions;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.Routing;
using WebApi.Basic.Controllers;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;

namespace WebApi.Basic
{
    public abstract class WebApiBase : ApiController
    {
        private static IHttpRoute defaultRoute;
        public const string DefaultRouteName = "DefaultApi";
        public const string QualifiedRouteName = "QualifiedRoute";
        private static readonly Regex badValueExpression = new Regex(@"Invalid enum value '(?<value>[^']+)' cannot be deserialized");

        public static void Initialize(HttpConfiguration configuration)
        {
            //configuration.Formatters.JsonFormatter.UseDataContractJsonSerializer = true;
            configuration.Formatters.JsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            configuration.Formatters.JsonFormatter.SerializerSettings.Converters.Add(new StringEnumConverter());
            configuration.Formatters.JsonFormatter.SerializerSettings.DateFormatHandling = DateFormatHandling.IsoDateFormat;
            configuration.Formatters.JsonFormatter.SerializerSettings.DateTimeZoneHandling = DateTimeZoneHandling.RoundtripKind;
            
            configuration.Formatters.Remove(configuration.Formatters.XmlFormatter);

            configuration.IncludeErrorDetailPolicy = IncludeErrorDetailPolicy.Always;

#if DEBUG
            configuration.Formatters.JsonFormatter.Indent = true;
#endif

            configuration.Formatters.JsonFormatter.MediaTypeMappings.Add(new UriPathExtensionMapping("json", "application/json"));
            
            defaultRoute = configuration.Routes.MapHttpRoute(name: DefaultRouteName, routeTemplate: "api/v1/{controller}/{id}", defaults: new { id = RouteParameter.Optional });
            

            configuration.Filters.Add(new ExpectedExceptionsFilterAttribute());

        }
        protected string GetLocation(string id)
        {
            return GetLocation(id, DefaultRouteName);
        }

        protected string GetLocation(string id, string routeName)
        {
            var controller = Request.GetRouteData().Values["controller"].ToString();
            return GetLocation(id, routeName, controller, Url);
        }

        protected static string GetLocation(string id, string routeName, string controller, UrlHelper url)
        {
            return url.Link(routeName, new { controller = controller, id = id });
        }

        protected string GetLocation(string routeName, dynamic routeValues)
        {
            return Url.Link(routeName, routeValues);
        }

        protected string GetIdFromDefaultRoutUrl(string href, string expectedController)
        {
            var routeData = defaultRoute.GetRouteData(Configuration.VirtualPathRoot, new HttpRequestMessage(HttpMethod.Get, href));
            if (routeData != null && routeData.Values["controller"] as string == expectedController)
            {
                return routeData.Values["id"] as string;
            }
            return null;
        }

        protected HttpResponseMessage HandleValidationErrors(string parameterName)
        {
            var appModelStates =
                ModelState.Where(pair => pair.Key.StartsWith(parameterName, StringComparison.InvariantCultureIgnoreCase));
            var errors = from state in appModelStates
                         from error in state.Value.Errors
                         let propertyName =
                             state.Key.Contains(".")
                                 ? state.Key.Substring(state.Key.IndexOf(".") + 1)
                                 : null
                         let message = GetErrorMessage(error, propertyName)
                         select message;


            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, string.Join("\n", errors));
        }       

        private static string GetErrorMessage(ModelError error, string propertyName)
        {
            var message = string.IsNullOrWhiteSpace(error.ErrorMessage)
                              ? error.Exception.Message
                              : error.ErrorMessage;
            if (badValueExpression.IsMatch(message))
            {
                message = string.Format("Invalid value '{0}'",
                                        badValueExpression.Match(message).Groups["value"].Value);
            }
            return propertyName != null
                       ? string.Format("There was an error validating the property '{0}'. {1}", propertyName, message)
                       : string.Format("There was an error validating a property. {0}", message);
        }
    }
}
