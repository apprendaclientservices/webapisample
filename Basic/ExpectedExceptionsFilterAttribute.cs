﻿using System.ServiceModel;
using Apprenda;
using Apprenda.Services.Logging;
using WebApi.Basic.Controllers;

namespace WebApi.Basic
{
    using System.Net;
    using System.Net.Http;
    using System.Web.Http.Filters;

    public class ExpectedExceptionsFilterAttribute : ExceptionFilterAttribute
    {
        private static readonly ILogger logger = LogManager.Instance().GetLogger(typeof (ExpectedExceptionsFilterAttribute));

        public override void OnException(HttpActionExecutedContext context)
        {
            var applicationFault = context.Exception as FaultException<ApplicationFaultDetail>;
            if (applicationFault != null)
            {
                context.Response = context.Request.CreateErrorResponse(applicationFault.Detail.HttpStatusCode, applicationFault.Detail.EndUserMessage);
            }
            else
            {
                var controllerException = context.Exception as RestControllerException;
                if (controllerException != null)
                {
                    context.Response = controllerException.CreateErrorResponse(context.Request);
                }
                else
                {
                    var systemFault = context.Exception as FaultException<SystemFaultDetail>;
                    if (systemFault != null && systemFault.Detail.HttpStatusCode != null)
                    {
                        context.Response = context.Request.CreateErrorResponse(systemFault.Detail.HttpStatusCode.Value, systemFault.Message);
                    }
                    else
                    {
                        logger.Error("Unhandled exception in REST API", context.Exception);
                        context.Response = context.Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "An unexpected error occurred, see logs for more details.");
                    }
                }
            }
        }
    }
}