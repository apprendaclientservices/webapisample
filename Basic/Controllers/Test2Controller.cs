﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Routing;
using WebApiSample.Data;

namespace WebApi.Basic.Controllers
{
    public class Test2Controller : WebApiBase
    {
        public HttpResponseMessage Get()
        {
            return Request.CreateResponse(HttpStatusCode.OK, new SampleObject() { Id = 2, Value = "Hello Again!" });

        }

        public static string GetLocation(string appAlias, UrlHelper url)
        {
            return GetLocation(appAlias, WebApiBase.DefaultRouteName, "test2", url);
        }
    }
}
