﻿using System.Net;
using System.Net.Http;
using System.Web.Http.Routing;
using WebApiSample.Data;

namespace WebApi.Basic.Controllers
{
    public class TestController : WebApiBase
    {
        public HttpResponseMessage Get()
        {
            return Request.CreateResponse(HttpStatusCode.OK, new SampleObject(){Id=1, Value = "Hello World!"});
            
        }


        public static string GetLocation(string appAlias, UrlHelper url)
        {
            return GetLocation(appAlias, WebApiBase.DefaultRouteName, "test", url);
        }
    }
}
