﻿using System;

namespace WebApi.Basic.Controllers
{
    using System.Net;
    using System.Net.Http;

    public class RestControllerException : Exception
    {
        public HttpStatusCode HttpStatusCode { get; private set; }
        public RestControllerException(HttpStatusCode statusCode, string message)
            : base(message)
        {
            HttpStatusCode = statusCode;
        }

        public HttpResponseMessage CreateErrorResponse(HttpRequestMessage Request)
        {
            return Request.CreateErrorResponse(HttpStatusCode, Message);
        }
    }
}
